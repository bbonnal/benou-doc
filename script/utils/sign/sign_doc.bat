:: Called from PowerShell  
:: "$(PAGE_NB_SIGN)" "$(POSX)" "$(POSY)" "$(NAME_PART)"  

:: Change Drive and  to File Directory  
echo %~d1  

:: Run Cleanup  
call:cleanup  

::pdflatex "latex_overlay_sign.tex"
:: Run pdflatex -&gt; bibtex -&gt; pdflatex -&gt; pdflatex  
pdflatex "\def\InputFilename{%4.pdf} \def\InputPageSign{%1} \def\SignPosX{%2} \def\SignPosY{%3} \input{latex_overlay_sign.tex}"

del "%4_sign.pdf"

rename "latex_overlay_sign.pdf" "%4_sign.pdf"

del "latex_overlay_sign.pdf"

:: Run Cleanup  
call:cleanup  

:: Open PDF  
START "" "C:\Users\bonnal\AppData\Local\SumatraPDF\SumatraPDF.exe" "%4_sign.pdf" -reuse-instance  
:: Cleanup Function  
:cleanup  
del *.dvi
del *.out
del *.log 
del *.aux  
del *.bbl    
del *.blg  
del *.brf  

goto:eof  