:: Called from PowerShell  
:: 

:: Change Drive and  to File Directory  
echo %~d1  

:: Run Cleanup  
call:cleanup  

pdflatex "latex_safe_extract.tex"
:: Run pdflatex -&gt; bibtex -&gt; pdflatex -&gt; pdflatex  
pdflatex "\def\InputFilename{%1.pdf} \input{latex_safe_extract.tex}"

del "%1_safe.pdf"

rename "latex_safe_extract.pdf" "%1_safe.pdf"

del "latex_safe_extract.pdf"

:: Run Cleanup  
call:cleanup  

:: Open PDF  
START "" "C:\Users\bonnal\AppData\Local\SumatraPDF\SumatraPDF.exe" "%1_safe.pdf" -reuse-instance  
:: Cleanup Function  
:cleanup  
del *.dvi
del *.out
del *.log 
del *.aux  
del *.bbl    
del *.blg  
del *.brf  

goto:eof  