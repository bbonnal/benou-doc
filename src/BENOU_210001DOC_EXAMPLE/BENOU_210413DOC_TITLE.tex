\documentclass[]{../../commons/tex/latex/benou/benou}

\usepackage{lipsum}
\usepackage{tabularx}

\doctitle{Documentation}
\title{\texttt{benou.cls}\\document class}	     	% 
%report title
\author{Benjamin Bonnal}										% report author

\docid{
code = BENOU,
sequence = 1,
subcode = ,
year = 2022,
type = documentation,
revision = 0.1}

% \deprecated[2017-01-01]
% \superseded[MOPI-xxx]

\date{21}{12}{2022}


\begin{document}
	\maketitle
	
	\begin{introduction}
Communication is key to a performing team. In  addition to having a good 
communication, a team must learn from previous mistakes and keep a trace of 
achieved work. Instant messaging services excels at allowing fast communication 
and quick discussion. However, they are not suited to long explanation and 
in-depth concept discussion. Furthermore, they are not well suited for storage 
of the past discussion (the free Slack edition only keeps the 10\,000 most 
recent messages).

This document presents the \texttt{motionpilot} \LaTeX document class, a 
standardized document format and classification system to ease communication 
and storage of documents.

To start using \verb|motionpilot| as a document class, simply write the 
following line at the top of your document: \verb|\documentclass{motionpilot}|.
	\end{introduction}
	
	\tableofcontents
	
	\section{Document Identifier}
A document identifier is used to uniquely and unambiguously identify a 
document. Such a number may later be used in an Enterprise Content Management 
software (ECM). The document identifier is formed with a prefix code, the 
current year and a sequence number in the current year, as well as the document 
type and the revision number. 

For example, a document is identified by:

\begin{center}
	\texttt{MOPI--17000DOC Rev.1}
\end{center}

\texttt{MOPI} is the prefix code. All MotionPilot documents should start with 
this prefix code. \texttt{17} is the current year. \texttt{0000} is the 
sequence number. This document is the first document using this identification 
system, thus it is the document number zero. \texttt{DOC} is the document type: 
\texttt{DOC}umentation. \texttt{Rev.1} is the document revision number. 

The document type is purely informational. The revision number, when omitted, 
is assumed to be the latest revision. One could simply refer to a document with 
its year number and sequence number. For example, \texttt{170001} stands for 
document \texttt{MOPI-17001TR Rev.2}.

Other examples:

\begin{center}
	\texttt{MOPI--17001TR Rev.2}\\
	\texttt{MOPI--18015PO Rev.1}\\
	\texttt{MOPI--99999XX Rev.9}\\
\end{center}

See Section~\ref{sec:doctype} for a list of all suggested document types and 
code.

If the sequence number is greater than \texttt{999} for the current year, one 
could start using letters as sequence number. This numbering allows up to 
45\,657 documents per year while keeping the alphabetic ordering.
For example, the series goes:
\begin{center}
\texttt{998}$\,\to\,$
\texttt{999}$\,\to\,$
\texttt{A00}$\,\to\,$
\texttt{A01}$\,\to\,$
\texttt{...}$\,\to\,$
\texttt{A99}$\,\to\,$
\texttt{B00}$\,\to\,$
\texttt{...}$\,\to\,$
\texttt{Z99}$\,\to\,$
\texttt{AA0}$\,\to\,$
\texttt{...}$\,\to\,$
\texttt{ZZZ}
\end{center}

The document year should not change even if a new revision is made in another 
year. For example, if a document is published in year 2017 with identifier 
\texttt{MOPI-17001TR Rev.1}, a subsequent revision in year 2018 should have 
identifier \texttt{MOPI-17001TR Rev.2}, not \texttt{MOPI-18001TR Rev.2}.

\section{Standard Document Template}
This section describes the standard document templates that shall be used with 
the \LaTeX{} class to help authors write a compliant document. The 
class is used by adding \verb|\documentclass{motionpilot}| at the beginning of 
the document.

\subsection{Document Identifier}\label{sec:docid}
The document id can be automatically generated using a \LaTeX{} command. For 
this document, the command is \verb|\docid{<params>}| where \verb|<params>| is 
a comma-separated list of key-value pairs. For this document, the command 
syntax to generate the document id \texttt{MOPI--170000DOC\space Rev.1} is:
\begin{verbatim}
\docid{
    code=MOPI,
    type=documentation,
    shorttype=DOC,
    year=2017,
    sequence=0,
    revision=1
}
\end{verbatim}

The available commands are:

\begin{tabular}{|l|l|}\hline
    \textbf{Key} & \textbf{Description} \\\hline

    \texttt{code} 
    & The document prefix code. \textbf{Default}: \texttt{MOPI}.\\\hline

    \texttt{type} 
    & The document type. \textbf{Required.}\\\hline
    
    \texttt{shorttype} 
    & The document type suffix. \textbf{Required} if \texttt{documenttype} is 
    not a registered document type.\\\hline
    
    \texttt{year} 
    & The document year. \textbf{Required.}\\\hline
    
    \texttt{sequence} 
    & The sequence number. \textbf{Required.}\\\hline

    \texttt{revision} 
    & The revision number. \textbf{Required.}\\\hline
\end{tabular}

\subsection{Document Type}\label{sec:doctype}
A few document types are already defined and should be used as much as 
possible. However, on may define their own document type and short code. The 
short document type code should be two or three letters long and easy to 
understand and not already in use for another document type.
Here is the list of the pre defined document types and their short code and use 
case.

\begin{tabularx}{\textwidth}{lX}
	\texttt{generalannouncement} (\texttt{GA}) & A document that will benefit 
	the whole 
	team and future members. \\
	\texttt{technicalreport} (\texttt{TR}) & A document that describe a process,
	progress, state or result of a technical or scientific research. \\
	\texttt{engineeringpolicy} (\texttt{EP}) & A document that establishes the 
	rules for designing new product and more general guidelines for engineering 
	related guidelines. \\
	\texttt{documentation} (\texttt{DOC}) & A document for documenting. 
	Technical report should be preferred whenever possible. \\
	\texttt{proposal} (\texttt{PR}) & A document proposing new documents, 
	policies...\\
	\texttt{procesverbal} (\texttt{PV}) & The minutes from a meeting with all 
	the decisions.\\
	\texttt{ordredujour} (\texttt{OJ}) & Ordre du jour for a meeting.\\
   	\texttt{weeklyreport} (\texttt{WR}) & Weekly report.
\end{tabularx}

The document title must match the document type and type code. One can set the 
document title using the command: \verb|\doctitle{<title>}|. If it is not set, 
an error will be raised.

\subsection{``Not For Publication'' Notice}\label{sec:notforpub}
If a document is for internal use only, a ``Not For Publication'' notice should 
be added to the document. In the \LaTeX{} template, one must add 
\verb|\notforpublication| in the preamble of the document. This command adds 
the following text on the first page of the document:
\begin{center}
	\textbf{\large Important notice:\\ this document is for 
		internal use only and is not intended\\ for distribution to the  
		public.}
\end{center}

The command also adds the \textbf{Not For Publication} notice in the footer of 
all subsequent pages.

\subsection{Watermark}
A watermark can be added using document class options. To add the 
\texttt{DRAFT} watermark, one must add the \texttt{wdraft} options to the 
document class: \verb|\documentclass[wdraft]{motionpilot}|. For 
\texttt{CONFIDENTIAL} watermark, the \texttt{wcondifential} watermark can be 
used. The use of the \texttt{confidential} watermark should be reserved to 
sensitive information relative to the technology or strategy of MotionPilot, 
such as a document about domain names purchases. One can define its own 
watermark using the included package \texttt{draftwatermark}. 

\subsection{Title Page}
The first page of the document must have the MotionPilot logo, the document 
identifier, the document type, the document title and the last modification 
date, and an environment-friendly notice. If applicable, the authors may also 
be added to the title page. Authors should not be added in some cases, such as 
in a datasheet.

The authors are set using the \verb|\author{<author>}| command. If there are 
multiple authors, they must be sorted by the amount of work and 
alphabetically if no clear order can be established between two or more author. 
All authors of a document must have provided a significant enough amount of 
work to be included. The authoring information is important to inform the 
public who to contact about the publicly released document.
Examples:
\begin{verbatim}
   \author{Timoth\'ee Peter}
   \author{Thibaut Paschal and Timoth\'ee Peter}
   \author{Benjamin Bonnal, Thibaut Paschal and Timoth\'ee Peter}
\end{verbatim}

The document type in the title is set using  \verb|\doctitle{<type>}| as 
described in Section~\ref{sec:doctype}. The document identifier is set using 
the \verb|\docid{<params>}| command described in Section~\ref{sec:docid}. The 
document title is set using the \verb|\title{<title>}| command. Two lines 
titles are tolerated using \texttt{\textbackslash\textbackslash} e.g.
\texttt{\textbackslash{}title\{First line\textbackslash\textbackslash{}Second 
line\}}.

The title page also includes an introduction or abstract. This introduction 
text is added using the environment \verb|\introduction[<title>]|. The default 
title \textbf{Introduction} may be changed using the optional parameter:
\begin{verbatim}
    \begin{introduction}[Abstract]
        content...
    \end{introduction}
\end{verbatim}

An important ``Not For Publication'' notice shall be added on the first page if 
the document is not intended for distribution to the public. As described in 
Section~\ref{sec:notforpub}, one must use the \verb|\notforpublication| command 
to add the notice.

\subsection{Header and Footer}
The header and footer are automatically managed by the document class. They 
must include the following information:
\begin{itemize}
	\item Document identifier
	\item Date of last revision
	\item ``Not For Publication'' notice if need be
	\item Page number and total page number
	\item Current section (optional)
\end{itemize}

\subsection{Deprecated and Superseded Commands}
If the document is deprecated, use the \verb|\deprecated[<since>]| command 
where \verb|<since>| is an optional deprecation date. Here is the result with 
and without the deprecation date:

\textcolor{red}{\textbf{THIS DOCUMENT IS DEPRECATED SINCE JUNE 2017. FOR 
REFERENCE ONLY.}}\\
\textcolor{red}{\textbf{THIS DOCUMENT IS DEPRECATED. FOR REFERENCE ONLY.}}

If the document is superseded, use the \verb|\superseded[<by>]| command where 
\verb|<by>| is an optional document name. Here is the result with and 
without the superseding document name:

\textcolor{red}{\textbf{THIS DOCUMENT IS SUPERSEDED BY MOPI-XXXX. FOR 
		REFERENCE ONLY.}}\\
\textcolor{red}{\textbf{THIS DOCUMENT IS SUPERSEDED. FOR REFERENCE ONLY.}}

The red warnings are added on the title page.

\subsection{Supplementary material}
If supplementary material is required, such as Gerber files, the document must 
clearly state a way to obtain this supplementary material. For publicly 
available document, the supplementary material must be publicly available as 
well.

\subsection{Document Information}
The last section of a document may list the revisions history. For each 
revision it must state the revision number, the date of the last modification 
of the revision and a description with all changes between the previous 
revision and the current one. This will be added as a command in a later update 
of the document class. 

\subsection{Other Important Pages}
If applicable, the document may include a table of contents, a list of figure, 
a glossary, a list of tables, a list of equations... In addition, it may
include a legal notice page which is automatically handled by the \LaTeX{}
template. The legal notice page may be removed with the command 
\verb|\nodisclaimer|.

\subsection{Document Class Options}
Use the \verb|short| options for document with a small number of pages. No 
table of content must be included, nor any list of tables, figures... This will 
change the layout to be more suited for a few pages document, such as a weekly 
report.

\section{Additional Features}
\subsection{Social Media Report}
A social media report may be included using 
the command \verb|\socialmedia{<filename>}| where \verb|<filename>| is the 
file name of the report generated by MotionPilot's scraper without the 
\texttt{.tex} extension. The file must be 
located in the same directory as the main document \LaTeX{} file.

To generate a report in \LaTeX{} format using MotionPilot's scraper, the 
following command should be used:
\begin{verbatim}
    python scraper.py --scrap-all --output-format latex
\end{verbatim}
The generated file is named \texttt{report.tex}. More on that later.

\section{Writing Guidelines}
The document should be preferably written in English, but French is acceptable 
as well.

\section{Publishing}

\section{Document Information}
\subsection{Revisions}
\begin{tabularx}{\textwidth}{|l|l|X|}\hline
	\textbf{Revision}&\textbf{Date}&\textbf{Description} \\\hline
		0 & \today & Initial draft \\\hline
\end{tabularx}
\end{document}
