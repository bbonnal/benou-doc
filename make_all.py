import os
import shutil
import subprocess
from subprocess import check_output, CalledProcessError
import requests
import zipfile
import sys

# Configuration
recompile = False


keyword_for_main_tex = "BENOU_"
date_format = "YYMMDD"
repo_path = "/builds/bbonnal/benou-doc"
site_pdf_path = os.path.join(repo_path, "site/static/pdf/")
site_post_path = os.path.join(repo_path, "site/content/posts/")
artifacts_output_path = "/builds/bbonnal/benou-doc/artifacts"  # Temporary path for artifacts
GITLAB_API_URL = "https://gitlab.com/api/v4"
GITLAB_PROJECT_ID = "25864353"
GITLAB_JOB_NAME = "make_all"
GITLAB_ACCESS_TOKEN = None

def get_last_successful_job(project_id, job_name, access_token):
    headers = {"PRIVATE-TOKEN": access_token}
    jobs_url = f"{GITLAB_API_URL}/projects/{project_id}/jobs"
    
    params = {
        "scope": "success",
        "per_page": 100
    }
    
    print("Request URL:", jobs_url)
    print("Headers:", headers)
    response = requests.get(jobs_url, headers=headers, params=params)
    print("Response Code:", response.status_code)
    print("Response Content:", response.text)
    
    response.raise_for_status()
    jobs = response.json()
    
    for job in jobs:
        if job["name"] == job_name:
            return job["id"], job["artifacts_file"]["filename"]
    return None, None

def download_artifacts(project_id, job_id, access_token, output_path):
    headers = {"PRIVATE-TOKEN": access_token}
    artifacts_url = f"{GITLAB_API_URL}/projects/{project_id}/jobs/{job_id}/artifacts"
    
    response = requests.get(artifacts_url, headers=headers, stream=True)
    response.raise_for_status()
    
    zip_path = os.path.join(output_path, "artifacts.zip")
    with open(zip_path, "wb") as f:
        for chunk in response.iter_content(chunk_size=8192):
            f.write(chunk)
    
    with zipfile.ZipFile(zip_path, 'r') as zip_ref:
        zip_ref.extractall(output_path)
    
    os.remove(zip_path)

def get_changed_files():
    try:
        diff_files = check_output(["git", "diff-tree", "--no-commit-id", "--name-only", "-r", os.environ["CI_COMMIT_SHA"]], cwd=repo_path)
        diff_files = diff_files.decode("utf-8").split("\n")
        return diff_files
    except CalledProcessError as e:
        print(f"Error retrieving changed files: {e}")
        return []

def compile_tex_files(tex_files):
    tex_root_directory = os.getcwd()
    for tex_file in tex_files:
        tex_file_dir = os.path.dirname(tex_file)
        tex_file_name = os.path.basename(tex_file)
        if tex_file_name.startswith(keyword_for_main_tex):
            os.chdir(os.path.join(repo_path, tex_file_dir))
            os.system(f"latexmk -lualatex {tex_file_name}")
            os.chdir(tex_root_directory)

def copy_and_create_md(pdf_files):
    for pdf_file in pdf_files:

        pdf_dir = os.path.dirname(pdf_file)
        pdf_name = os.path.basename(pdf_file)
        
        if os.path.exists(pdf_file):
            # Create the markdown file for the Hugo site
            md_file = os.path.join(site_post_path, os.path.splitext(pdf_name)[0] + '.md')
            with open(md_file, 'w') as f:
                print(f"New file: {md_file}")
                f.write("\n")
                f.write("+++\n")
                f.write(f"author = \"Benou\"\n")
                f.write(f"title = \"{os.path.splitext(pdf_name)[0]}\"\n")
                f.write(f"date = \"{os.path.splitext(pdf_name)[0][len(keyword_for_main_tex):len(date_format)]}\"\n")
                f.write("description = \"Test embedded pdf\"\n")
                f.write("tags = [\n")
                f.write("    \"pdf\",\n")
                f.write("]\n")
                f.write("+++\n")
                f.write("\n")
                f.write(f"{{{{< pdfReader \"../pdf/{pdf_name}\" >}}}}\n")

def move_pdf_publish(dir):
    for root, _, files in os.walk(dir):
        for file in files:
            if file.endswith(".pdf") and file.startswith(keyword_for_main_tex):
                src_file = os.path.join(root, file)
                dst_file = os.path.join(site_pdf_path, file)
                # Prevent overwriting of the pdf by the deprecated ones
                if not os.path.exists(dst_file):
                    try:
                        shutil.copy(src_file, dst_file)
                        print("This file")
                        print(src_file)
                        print("Was copied here")
                        print(dst_file)
                    except:
                        print("This file is already here")
                        print(dst_file)


if __name__ == "__main__":
    tex_root_directory = os.getcwd()
    os.system(f"git config --global --add safe.directory {repo_path}")

    if not os.path.exists(artifacts_output_path):
        os.makedirs(artifacts_output_path)

    GITLAB_ACCESS_TOKEN=sys.argv[1]

    print("Access token")
    print(GITLAB_ACCESS_TOKEN)
    
    job_id, artifacts_filename = get_last_successful_job(GITLAB_PROJECT_ID, GITLAB_JOB_NAME, GITLAB_ACCESS_TOKEN)
    
    # if job make_all exists try to fetch all .pdf artifacts
    if job_id and not recompile:
        changed_files = get_changed_files()
        tex_files = [file for file in changed_files if file.endswith(".tex")]
    # otherwise recompile everything
    else:
        print(f"No successful job found for job name '{GITLAB_JOB_NAME}'.")
        print(f"Set every .tex for recompile.")
        try:
            tex_files = check_output(["git", "ls-tree", "-r", "--name-only", "HEAD"], cwd=repo_path)
            tex_files = tex_files.decode("utf-8").split("\n")
            # Filter only .tex files
            tex_files = [file for file in tex_files if file.endswith(".tex")]
            # Optional: If you want to exclude empty strings resulting from split
            tex_files = [file for file in tex_files if file]  # Removes empty strings
        except CalledProcessError as e:
            print(f"Error retrieving all tex files: {e}")


    # compile the necessary .tex
    compile_tex_files(tex_files)

    # Move the freshly compiled pdf to place
    move_pdf_publish(repo_path)

    # Download last succesfull artifacts package
    download_artifacts(GITLAB_PROJECT_ID, job_id, GITLAB_ACCESS_TOKEN, artifacts_output_path)

    # Copy those into place
    move_pdf_publish(artifacts_output_path)

    # grab a list of all pdfs in the publish dir
    out_files = []
    for dirpath, dirnames, filenames in os.walk(site_pdf_path):
        for file in filenames:
            if file.endswith(".pdf") and file.startswith(keyword_for_main_tex):
                out_files.append(os.path.join(dirpath, file))

    print("Will generate .md for the following files")
    print(out_files)

    # make .md posts for each pdfs
    copy_and_create_md(out_files)
